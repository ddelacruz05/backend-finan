FROM openjdk:21-ea-11-jdk
ADD target/financiero-1.0.0.jar financiero-1.0.0.jar
ENV DEFAULT_OPTIONS="-Duser.timezone=America/Guayaquil -Djava.net.preferIPv4Stack=true -Djava.security.egd=file:/dev/./urandom"
ENV JAVA_OPTS=""
ENTRYPOINT java ${DEFAULT_OPTIONS} ${JAVA_OPTS} -jar financiero-1.0.0.jar

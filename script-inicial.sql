CREATE SEQUENCE seq_persona
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999999
  START 1;

CREATE SEQUENCE seq_cliente
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999999
  START 1;

CREATE SEQUENCE seq_cuenta
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999999
  START 1;

CREATE SEQUENCE seq_movimiento
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 99999999
  START 1;


Para levantar y validar el componente se debe considerar lo siguiente:

1.  Tener instalado:
    Java versión >= 11
    Maven versión >= 3.8.1
    Postman
    Docker >= 20.10.14

2. Se debe clonar el repositorio en una carpeta a su elección que tenga permisos de escritura y lectura:

```
git clone https://gitlab.com/ddelacruz05/backend-finan.git
```

3. Se debe compilar el proyecto desde la consola en el directorio raíz del componente con el comando:

```
mvn clean install -Dmaven.test.skip=true
```

4. Se debe levantar los contenedores con la base de datos ejecutanto en el directorio raíz del componente, con el siguiente comando:

```
docker-compose up -d

```

5. Importar la colección  en Postman y validar las APIS con el archivo financiero.postman_collection.json


package com.data.financiero.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.financiero.dto.ClienteDto;
import com.data.financiero.dto.RespuestaDTO;
import com.data.financiero.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

  @Autowired
  ClienteService clienteServ;

  @PostMapping(consumes = "application/json", produces = "application/json")
  public ResponseEntity<ClienteDto> crearCliente(@RequestBody ClienteDto clienteDto) {
    return new ResponseEntity<>(clienteServ.guardar(clienteDto), HttpStatus.OK);
  }

  @GetMapping(produces = "application/json")
  public ResponseEntity<List<ClienteDto>> consultarClientes() {
    return new ResponseEntity<>(clienteServ.consultar(), HttpStatus.OK);
  }

  @DeleteMapping(produces = "application/json", path = "/{clienteId}")
  public ResponseEntity<RespuestaDTO> eliminarCliente(@PathVariable("clienteId") Long clienteId) {

    return new ResponseEntity<>(clienteServ.eliminar(clienteId), HttpStatus.OK);

  }

  @PutMapping(path = "/{clienteId}", consumes = "application/json")
  public ResponseEntity<ClienteDto> actualizarCliente(@PathVariable Long clienteId,
      @RequestBody ClienteDto clienteDto) {
    clienteDto.setId(clienteId);
    return new ResponseEntity<>(clienteServ.actualizar(clienteDto), HttpStatus.OK);
  }

}

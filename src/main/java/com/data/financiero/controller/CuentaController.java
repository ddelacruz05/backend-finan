package com.data.financiero.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.financiero.dto.CuentaDto;
import com.data.financiero.dto.CuentaRequestDto;
import com.data.financiero.dto.RespuestaDTO;
import com.data.financiero.service.CuentaService;

@RestController
@RequestMapping("/cuentas")
public class CuentaController {

    @Autowired
    CuentaService cuentaServ;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<CuentaDto> crearCuenta(@RequestBody CuentaRequestDto cuentaReq) {
        return new ResponseEntity<>(cuentaServ.guardar(cuentaReq), HttpStatus.OK);
    }

    @PutMapping(path = "/{numeroCuenta}", consumes = "application/json")
    public ResponseEntity<CuentaDto> actualizarCliente(@PathVariable("numeroCuenta") Long numeroCuenta,
            @RequestBody CuentaRequestDto cuentaDto) {
        cuentaDto.setNumeroCuenta(numeroCuenta);
        return new ResponseEntity<>(cuentaServ.actualizar(cuentaDto), HttpStatus.OK);
    }

    @DeleteMapping(produces = "application/json", path = "/{numeroCuenta}")
    public ResponseEntity<RespuestaDTO> eliminarCliente(@PathVariable("numeroCuenta") Long numeroCuenta) {
        return new ResponseEntity<>(cuentaServ.eliminar(numeroCuenta), HttpStatus.OK);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<CuentaDto>> consultarCuentas() {
        return new ResponseEntity<>(cuentaServ.consultar(), HttpStatus.OK);
    }
}

package com.data.financiero.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.data.financiero.dto.MovimientoDto;
import com.data.financiero.dto.EstadoCuentaDto;
import com.data.financiero.dto.MovimientoRequestDto;
import com.data.financiero.service.MovimientoService;

@RestController
@RequestMapping("/movimientos")
public class MovimientoController {

    @Autowired
    MovimientoService movimientoServ;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<MovimientoDto> crearMovimiento(@RequestBody MovimientoRequestDto movimientoReq) {
        return new ResponseEntity<>(movimientoServ.guardar(movimientoReq), HttpStatus.OK);
    }

    @GetMapping(produces = "application/json", path = "/reportes")
    public ResponseEntity<List<EstadoCuentaDto>> consultarMovimientos(
            @RequestParam(required = true) Long clienteId,
            @RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fechaInicio,
            @RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fechaFin) {
        return new ResponseEntity<>(movimientoServ.consultarEstadoCuenta(clienteId, fechaInicio, fechaFin),
                HttpStatus.OK);
    }
}

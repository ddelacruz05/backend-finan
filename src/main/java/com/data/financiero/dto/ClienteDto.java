package com.data.financiero.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ClienteDto extends PersonaDto{
    private String contrasenia;
    private boolean estado;
}

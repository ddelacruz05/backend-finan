package com.data.financiero.dto;

import lombok.Data;

@Data
public class RespuestaDTO {

	private String mensaje;
}

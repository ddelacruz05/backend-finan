package com.data.financiero.dto;

import lombok.Data;

@Data
public class CuentaRequestDto {
    private Long numeroCuenta;
    private String tipoCuenta;
    private Double saldoInicial;
    private boolean estado;
    private Long clienteId;
}

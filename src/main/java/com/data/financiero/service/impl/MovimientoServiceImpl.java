package com.data.financiero.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.financiero.dto.MovimientoDto;
import com.data.financiero.dto.EstadoCuentaDto;
import com.data.financiero.dto.MovimientoRequestDto;
import com.data.financiero.entity.Cliente;
import com.data.financiero.entity.Cuenta;
import com.data.financiero.entity.Movimiento;
import com.data.financiero.exception.FinancieroException;
import com.data.financiero.repository.CuentaRepository;
import com.data.financiero.repository.MovimientoRepository;
import com.data.financiero.service.MovimientoService;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CuentaRepository cuentaRepo;

    @Autowired
    private MovimientoRepository movimientoRepo;

    @Override
    public MovimientoDto guardar(MovimientoRequestDto movimientoReq) {
        Movimiento movimientoEntity = modelMapper.map(movimientoReq, Movimiento.class);
        log.info(movimientoReq.getTipoCuenta());
        Optional<Cuenta> cuentaOpt = cuentaRepo.findByNumeroCuentaAndTipoCuenta(movimientoReq.getNumeroCuenta(),
                movimientoReq.getTipoCuenta());
        if (cuentaOpt.isPresent() && cuentaOpt.get().isEstado()) {
            movimientoEntity.setCuenta(cuentaOpt.get());
        } else {
            throw new FinancieroException("La cuenta no existe");
        }
        if (!movimientoEntity.getTipoMovimiento().equals("Retiro")
                && !movimientoEntity.getTipoMovimiento().equals("Deposito")) {
            throw new FinancieroException("Tipo Movimiento no existe");
        }
        Optional<Movimiento> movimientoOpt = movimientoRepo.findFirstByCuentaOrderByFechaDesc(cuentaOpt.get());

        Double nuevoSaldo = 0.0;
        if (movimientoOpt.isPresent()) {
            Movimiento ultimoMovimiento = movimientoOpt.get();
            if (ultimoMovimiento.getSaldo() == 0 && movimientoEntity.getTipoMovimiento().equals("Retiro")) {
                throw new FinancieroException("Saldo no disponible");
            }
            if (movimientoEntity.getTipoMovimiento().equals("Retiro")) {
                nuevoSaldo = ultimoMovimiento.getSaldo() - movimientoEntity.getValor();
                if (nuevoSaldo < 0) {
                    throw new FinancieroException("Saldo insuficiente para la transacción");
                }
            }
            if (movimientoEntity.getTipoMovimiento().equals("Deposito")) {
                nuevoSaldo = ultimoMovimiento.getSaldo() + movimientoEntity.getValor();
            }
            movimientoEntity.setSaldo(nuevoSaldo);
        }

        Movimiento movimientoNuevo = movimientoRepo.save(movimientoEntity);

        return modelMapper.map(movimientoNuevo, MovimientoDto.class);
    }

    @Override
    public List<EstadoCuentaDto> consultarEstadoCuenta(Long clienteId, Date fechaInicio, Date fechaFin) {
        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        List<Movimiento> movimientos = movimientoRepo.findAllByCuentaClienteAndFechaBetween(cliente,fechaInicio, fechaFin);
        return mapearEstadoCuenta(movimientos);
    }

    private List<EstadoCuentaDto> mapearEstadoCuenta(List<Movimiento> movimientos){
        return movimientos.stream()
                .map(movimiento -> {
                    EstadoCuentaDto estadoCuentaDto = new EstadoCuentaDto();
                    estadoCuentaDto.setFecha(movimiento.getFecha());
                    estadoCuentaDto.setCliente(movimiento.getCuenta().getCliente().getNombre());
                    estadoCuentaDto.setNumeroCuenta(movimiento.getCuenta().getNumeroCuenta());
                    estadoCuentaDto.setTipo(movimiento.getCuenta().getTipoCuenta());
                    estadoCuentaDto.setSaldoInicial(obtenerSaldoInicial(movimiento));
                    estadoCuentaDto.setEstado(movimiento.getCuenta().isEstado());
                    estadoCuentaDto.setMovimiento(movimiento.getValor());
                    estadoCuentaDto.setSaldoDisponible(movimiento.getSaldo());
                    return estadoCuentaDto;
                })
                .collect(Collectors.toList());
    }

    private Double obtenerSaldoInicial(Movimiento movimiento) {
        return movimiento.getTipoMovimiento().equals("Deposito") ?
                            movimiento.getSaldo() - movimiento.getValor() : 
                            movimiento.getSaldo() + movimiento.getValor();
    }

}

package com.data.financiero.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.financiero.dto.ClienteDto;
import com.data.financiero.dto.RespuestaDTO;
import com.data.financiero.entity.Cliente;
import com.data.financiero.exception.FinancieroException;
import com.data.financiero.repository.ClienteRepository;
import com.data.financiero.service.ClienteService;
import com.data.financiero.validator.ClienteValidator;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired
	private ClienteValidator clienteValid;

	@Override
	public ClienteDto guardar(ClienteDto cliente) {
		clienteValid.validarNulosVacios(cliente);
		Cliente clienteEntity = modelMapper.map(cliente, Cliente.class);
		Cliente clienteNuevo = clienteRepo.save(clienteEntity);
		return modelMapper.map(clienteNuevo, ClienteDto.class);
	}

	@Override
	public ClienteDto actualizar(ClienteDto cliente) {
		clienteValid.validarNulosVacios(cliente);
		Cliente clienteEntity = modelMapper.map(cliente, Cliente.class);
		Cliente clienteNuevo = clienteRepo.save(clienteEntity);
		return modelMapper.map(clienteNuevo, ClienteDto.class);
	}

	@Override
	public ClienteDto editar(ClienteDto cliente) {
		Cliente clienteEntity;
		Optional<Cliente> clienteOP = clienteRepo.findById(cliente.getId());
		if (clienteOP.isPresent()) {
			clienteEntity = clienteOP.get();
			clienteEntity.setContrasenia(
					cliente.getContrasenia() != null ? cliente.getContrasenia() : clienteEntity.getContrasenia());
			clienteEntity.setDireccion(
					cliente.getDireccion() != null ? cliente.getDireccion() : clienteEntity.getDireccion());
			clienteEntity.setEdad(cliente.getEdad() != null ? cliente.getEdad() : clienteEntity.getEdad());
			clienteEntity.setGenero(cliente.getGenero() != null ? cliente.getGenero() : clienteEntity.getGenero());
			clienteEntity.setNombre(cliente.getNombre() != null ? cliente.getNombre() : clienteEntity.getNombre());
			clienteEntity
					.setTelefono(cliente.getTelefono() != null ? cliente.getTelefono() : clienteEntity.getTelefono());

			clienteEntity = clienteRepo.save(clienteEntity);
		} else {
			throw new FinancieroException(
					"No se encontro cliente con id " + cliente.getId());
		}
		return modelMapper.map(clienteEntity, ClienteDto.class);
	}

	@Override
	public RespuestaDTO eliminar(Long clienteId) {
		RespuestaDTO resultado = new RespuestaDTO();
		Optional<Cliente> cliente = clienteRepo.findById(clienteId);
		if (cliente.isPresent()) {
			Cliente clienteEntidad = cliente.get();

			if (clienteEntidad.isEstado()) {
				clienteEntidad.setEstado(false);
				clienteRepo.save(clienteEntidad);
				resultado.setMensaje("Cliente eliminado correctamente");
			} else {
				throw new FinancieroException(
						"El cliente con id " + clienteId + " ya se encuentra eliminado");
			}
		} else {
			throw new FinancieroException("No se encontro cliente con el  id " + clienteId);
		}
		return resultado;
	}

	@Override
	public List<ClienteDto> consultar() {
		List<Cliente> clientes = clienteRepo.findAll();
		return clientes.stream()
				.map(cliente -> modelMapper.map(cliente, ClienteDto.class))
				.collect(Collectors.toList());
	}

}

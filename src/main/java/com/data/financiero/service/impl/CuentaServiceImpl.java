package com.data.financiero.service.impl;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.financiero.dto.CuentaDto;
import com.data.financiero.dto.CuentaRequestDto;
import com.data.financiero.dto.MovimientoRequestDto;
import com.data.financiero.dto.RespuestaDTO;
import com.data.financiero.entity.Cliente;
import com.data.financiero.entity.Cuenta;
import com.data.financiero.exception.FinancieroException;
import com.data.financiero.repository.ClienteRepository;
import com.data.financiero.repository.CuentaRepository;
import com.data.financiero.service.CuentaService;
import com.data.financiero.service.MovimientoService;
import com.data.financiero.validator.CuentaValidator;

@Service
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CuentaValidator cuentaValid;

    @Autowired
    private ClienteRepository clienteRepo;

    @Autowired
    private CuentaRepository cuentaRepo;

    @Autowired
    private MovimientoService movimientoServ;

    @Override
    public CuentaDto guardar(CuentaRequestDto cuentaReq) {
        cuentaValid.validarNulosVacios(cuentaReq);
        Cuenta cuentaEntity = modelMapper.map(cuentaReq, Cuenta.class);
        Optional<Cliente> clienteOpt = clienteRepo.findById(cuentaReq.getClienteId());
        if (clienteOpt.isPresent()) {
            cuentaEntity.setCliente(clienteOpt.get());
        } else {
            throw new FinancieroException("El Cliente no existe");
        }
        Cuenta cuentaNueva = cuentaRepo.save(cuentaEntity);
        MovimientoRequestDto movimientoDto = new MovimientoRequestDto();
        movimientoDto.setFecha(new Date());
        movimientoDto.setTipoMovimiento("Deposito");
        movimientoDto.setValor(cuentaNueva.getSaldoInicial());
        movimientoDto.setSaldo(cuentaNueva.getSaldoInicial());
        movimientoDto.setNumeroCuenta(cuentaNueva.getNumeroCuenta());
        movimientoDto.setTipoCuenta(cuentaNueva.getTipoCuenta());
        movimientoServ.guardar(movimientoDto);
        return modelMapper.map(cuentaNueva, CuentaDto.class);
    }

    @Override
    public CuentaDto actualizar(CuentaRequestDto cuentaReq) {
        cuentaValid.validarNulosVacios(cuentaReq);
        Cuenta cuentaEntity = modelMapper.map(cuentaReq, Cuenta.class);
        Cuenta cuentaNueva = cuentaRepo.save(cuentaEntity);
        return modelMapper.map(cuentaNueva, CuentaDto.class);
    }

    @Override
    public RespuestaDTO eliminar(Long numeroCuenta) {
        RespuestaDTO resultado = new RespuestaDTO();
        Optional<Cuenta> cuenta = cuentaRepo.findByNumeroCuenta(numeroCuenta);
        if (cuenta.isPresent()) {
            Cuenta cuentaEntidad = cuenta.get();

            if (cuentaEntidad.isEstado()) {
                cuentaEntidad.setEstado(false);
                cuentaRepo.save(cuentaEntidad);
                resultado.setMensaje("Cuenta eliminada correctamente");
            } else {
                throw new FinancieroException(
                        "La cuenta con número " + numeroCuenta + " ya se encuentra eliminada");
            }
        } else {
            throw new FinancieroException("No se encontro cuenta con el número " + numeroCuenta);
        }
        return resultado;
    }

    @Override
    public List<CuentaDto> consultar() {
        List<Cuenta> cuentas = cuentaRepo.findAll();
        return cuentas.stream()
                .map(cuenta -> modelMapper.map(cuenta, CuentaDto.class))
                .collect(Collectors.toList());
    }
}

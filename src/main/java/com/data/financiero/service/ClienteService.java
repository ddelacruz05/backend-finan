package com.data.financiero.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.data.financiero.dto.ClienteDto;
import com.data.financiero.dto.RespuestaDTO;

@Service
public interface ClienteService {
    public ClienteDto guardar(ClienteDto cliente);

    public ClienteDto editar(ClienteDto cliente);

    public ClienteDto actualizar(ClienteDto cliente) ;
    
    public List<ClienteDto> consultar();

    public RespuestaDTO eliminar(Long clienteId);
}

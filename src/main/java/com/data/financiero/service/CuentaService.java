package com.data.financiero.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.data.financiero.dto.CuentaDto;
import com.data.financiero.dto.CuentaRequestDto;
import com.data.financiero.dto.RespuestaDTO;

@Service
public interface CuentaService {
    public CuentaDto guardar(CuentaRequestDto cuentaReq);

    public CuentaDto actualizar(CuentaRequestDto cuentaReq);

    public RespuestaDTO eliminar(Long numeroCuenta);
 
    public List<CuentaDto> consultar();
}

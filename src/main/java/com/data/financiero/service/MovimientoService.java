package com.data.financiero.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.data.financiero.dto.MovimientoRequestDto;
import com.data.financiero.dto.MovimientoDto;
import com.data.financiero.dto.EstadoCuentaDto;
import com.data.financiero.dto.MovimientoRequestDto;

@Service
public interface MovimientoService {
    public MovimientoDto guardar(MovimientoRequestDto movimientoReq);

    public List<EstadoCuentaDto> consultarEstadoCuenta(Long clienteId, Date fechaInicio, Date fechaFin);
}

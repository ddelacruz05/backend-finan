package com.data.financiero.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity(name = "clientes")
@Data
@EqualsAndHashCode(callSuper = true)
@PrimaryKeyJoinColumn(name = "cliente_id")
public class Cliente extends Persona {
   
    private String contrasenia;
   
    private boolean estado;

}

package com.data.financiero.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.data.financiero.entity.Cliente;
import com.data.financiero.entity.Cuenta;
import com.data.financiero.entity.Movimiento;

@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento, Long> {

    public Optional<Movimiento> findFirstByCuentaOrderByFechaDesc(Cuenta cuenta);

    List<Movimiento> findAllByCuentaClienteAndFechaBetween(Cliente cliente, Date fechaInicio, Date fechaFin);
}

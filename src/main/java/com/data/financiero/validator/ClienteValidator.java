package com.data.financiero.validator;

import org.springframework.stereotype.Component;

import com.data.financiero.dto.ClienteDto;
import com.data.financiero.exception.FinancieroException;

@Component
public class ClienteValidator {

    public void validarNulosVacios(ClienteDto clienteDto) {
        if (clienteDto.getIdentificacion() == null || clienteDto.getIdentificacion().equals("")) {
            throw new FinancieroException("Se requiere la identificación del Cliente");
        }
        if (clienteDto.getNombre() == null || clienteDto.getNombre().equals("")) {
            throw new FinancieroException("Se requiere el nombre del Cliente");
        }
        if (clienteDto.getEdad() == null || clienteDto.getEdad() <= 0) {
            throw new FinancieroException("Se requiere la edad del Cliente");
        }
        if (clienteDto.getDireccion() == null || clienteDto.getDireccion().equals("")) {
            throw new FinancieroException("Se requiere la dirección del Cliente");
        }
        if (clienteDto.getTelefono() == null || clienteDto.getTelefono().equals("")) {
            throw new FinancieroException("Se requiere el teléfono del Cliente");
        }
        if (clienteDto.getContrasenia() == null || clienteDto.getContrasenia().equals("")) {
            throw new FinancieroException("Se requiere la contraseña del Cliente");
        }
    }
}

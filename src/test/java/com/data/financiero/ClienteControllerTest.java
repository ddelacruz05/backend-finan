package com.data.financiero;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.data.financiero.controller.ClienteController;
import com.data.financiero.dto.ClienteDto;
import com.data.financiero.dto.RespuestaDTO;
import com.data.financiero.service.ClienteService;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {

    @Mock
    private ClienteService clienteServ;

    @InjectMocks
    private ClienteController clienteController;

    @Test
    public void testCrearCliente() {

        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setNombre("David De La Cruz Suarez");
        clienteDto.setDireccion("Isla Trinitaria Sur");
        clienteDto.setTelefono("042600388");
        clienteDto.setContrasenia("Abc123");
        clienteDto.setIdentificacion("1234567890");
        clienteDto.setEdad(33);
        clienteDto.setEstado(true);

        when(clienteServ.guardar(clienteDto)).thenReturn(clienteDto);

        ResponseEntity<ClienteDto> response = clienteController.crearCliente(clienteDto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clienteDto, response.getBody());
    }

    @Test
    public void testEditarCliente() {

        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setNombre("David Saul De La Cruz Suarez");
        clienteDto.setDireccion("Isla Trinitaria Sur Trinipuerto");
        clienteDto.setTelefono("042600387");
        clienteDto.setContrasenia("Abc123");
        clienteDto.setIdentificacion("1234567890");
        clienteDto.setEdad(33);
        clienteDto.setEstado(true);
        clienteDto.setId(1L);

        when(clienteServ.actualizar(clienteDto)).thenReturn(clienteDto);

        ResponseEntity<ClienteDto> response = clienteController.actualizarCliente(1L, clienteDto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clienteDto, response.getBody());
    }

    @Test
    public void testEliminarCliente() {

        RespuestaDTO respuestaDTO = new RespuestaDTO();
        respuestaDTO.setMensaje("Cliente eliminado correctamente");

        when(clienteServ.eliminar(1L)).thenReturn(respuestaDTO);

        ResponseEntity<RespuestaDTO> responseEntity = clienteController.eliminarCliente(1L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(respuestaDTO, responseEntity.getBody());
    }

    @Test
    public void testConsultarCliente() {
        List<ClienteDto> listaClientes = Arrays.asList(new ClienteDto());
        when(clienteServ.consultar()).thenReturn(listaClientes);

        ResponseEntity<List<ClienteDto>> responseEntity = clienteController.consultarClientes();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(listaClientes, responseEntity.getBody());
    }
}
package com.data.financiero;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.data.financiero.controller.CuentaController;
import com.data.financiero.dto.ClienteDto;
import com.data.financiero.dto.CuentaDto;
import com.data.financiero.dto.CuentaRequestDto;
import com.data.financiero.service.CuentaService;

@RunWith(MockitoJUnitRunner.class)
public class CuentaControllerTest {

    @Mock
    private CuentaService cuentaServ;

    @InjectMocks
    private CuentaController cuentaController;

    @Test
    public void testCrearCuenta() {

        CuentaRequestDto cuentaDto = new CuentaRequestDto();
        cuentaDto.setClienteId(1l);
        cuentaDto.setNumeroCuenta(12345l);
        cuentaDto.setSaldoInicial(100.0);
        cuentaDto.setTipoCuenta("Ahorro");
        cuentaDto.setEstado(true);

        CuentaDto cuentaNueva = new CuentaDto();
        ClienteDto cliente = new ClienteDto();
        cliente.setId(1l);
        cuentaNueva.setCliente(cliente);
        cuentaNueva.setNumeroCuenta(12345l);
        cuentaNueva.setSaldoInicial(100.0);
        cuentaNueva.setTipoCuenta("Ahorro");
        cuentaNueva.setEstado(true);

        when(cuentaServ.guardar(cuentaDto)).thenReturn(cuentaNueva);

        ResponseEntity<CuentaDto> response = cuentaController.crearCuenta(cuentaDto);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(cuentaNueva, response.getBody());
    }
}
